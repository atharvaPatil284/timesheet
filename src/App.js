
import './App.css';
import React, {useState, useEffect} from 'react';
import { nanoid } from 'nanoid';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Form } from 'react-bootstrap';
import axios from 'axios';

function App() {
  const [emails, setEmails] = useState([]);
  const [maintasks, setMainTask] = useState([]);
  const [subtasks, setSubTask] = useState([]);
  const [selectedEmail, setSelectedEmail] = useState('');
  const [selectedMainTask, setSelectedMainTask] = useState('');
  const [selectedSubTask, setSelectedSubTask] = useState('');
  const [actual, setAcutal] = useState(0);
  const emailpayload = {
    token: "cqdQDGJ-akHskW7UbwO6Yw",
    data: {
     
      email: "internetEmail",
     
      _repeat: 2
    }
};
const maintaskpayload = {
  token: "cqdQDGJ-akHskW7UbwO6Yw",
  data: {
   
    first_name: "nameFirst",
   
    _repeat: 4
  }
};

const subtaskpayload = {
  token: "cqdQDGJ-akHskW7UbwO6Yw",
  data: {
   
    last_name: "nameLast",
   
    _repeat: 4
  }
};


useEffect(()=>{
  axios({
    method: "post",
    url: "https://app.fakejson.com/q",
    data: emailpayload
  }).then(function(resp) {
    console.log(resp.data);
    setEmails(resp.data.map(emails=><option id={nanoid()} value={emails.email}>{emails.email}</option>));
  });
},[]);



function getMainTask(e){
  axios({
    method: "post",
    url: "https://app.fakejson.com/q",
    data: maintaskpayload
  }).then(function(resp) {
    
    setMainTask(resp.data.map(maintask=><option id={nanoid()} value={maintask.first_name}>{maintask.first_name}</option>));
    setSelectedEmail(e.target.value);
  });
}

function getSubtask(e){
  axios({
    method: "post",
    url: "https://app.fakejson.com/q",
    data: subtaskpayload
  }).then(function(resp) {
    
    setSubTask(resp.data.map(subtask=><option id={nanoid()} value={subtask.last_name}>{subtask.last_name}</option>));
    console.log(e.target.value);
    setSelectedMainTask(e.target.value);
  });
}

function handleSubTask(e){
  setSelectedSubTask(e.target.value);
}

function handleActual(e){
  setAcutal(e.target.value);
}

function submitForm(){
  const data={
    email:selectedEmail,
    maintask:selectedMainTask,
    subtask:selectedSubTask,
    actual:actual,
  };


  axios({
    method: "post",
    url: "http://127.0.0.1:8000/home/",
    data: data,
    
  }).then(function(resp) {
    
    console.log(resp.data);
  });
  
}
  return (
    <div className="App" >
      <h1>Timesheet</h1>
      <Form>
        <Form.Label>Email</Form.Label>
        <Form.Control as="select" onChange={getMainTask}>
        <option id={nanoid()}>....</option>
          {emails}
        </Form.Control>
        <Form.Label>Main Tasks:</Form.Label>
        <Form.Control as="select" onChange={getSubtask}>
        <option id={nanoid()}>....</option>
          {maintasks}
        </Form.Control>
        <Form.Label>Sub Tasks:</Form.Label>
        <Form.Control as="select">
          {subtasks}
        </Form.Control>
        <Form.Label>Estimated:</Form.Label>
        <Form.Control
            required
            type="number"
            placeholder="Estimated"
            
          />
        <Form.Label>Actual:</Form.Label>
        <Form.Control
            required
            type="number"
            placeholder="Actual"
            onChange={handleActual}
          /><br></br>
          <Button variant="dark" onClick={submitForm}>Submit</Button>
      </Form>
      {/* <label for="email">Email: </label>
      <select id="email" name="email" onChange={getMainTask}>
        <option id={nanoid()}>....</option>
        {emails}
      </select><br></br>
      <label for="maintask">Maintask: </label>
      <select id="maintask" name="maintask"onChange={getSubtask}>
      <option id={nanoid()}>...</option>
      {maintasks}
      </select><br></br>
      <label for="subtask">Subtask: </label>
      <select id="subtask" name="subtask" onChange={handleSubTask}>
        <option id={nanoid()}>...</option>
        {subtasks}
      </select><br></br>
      <label for="estimated">Estimated: </label>
      <input type="number" id="estimated" name="estimated"></input><br></br>
      <label for="actual">actual: </label>
      <input type="number" id="actual" name="actual" onChange={handleActual}></input><br></br>

      <Button variant="dark" onClick={submitForm}>Submit</Button>
       */}

    </div>
  );
}

export default App;
