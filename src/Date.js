import axios from 'axios';
import { nanoid } from 'nanoid';
import React, {useState, useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table} from 'react-bootstrap';

export default function Date() {
    const [months, setMonths] = useState([]);
    const [years, setYears] = useState([]);
    const [rows, setRows] = useState([]);
    const [selectedyear, setSelectedYear] = useState(0);
    const [selectedmonth, setSelectedMonth] = useState("");
    useEffect(()=>{
        axios({
          method: "get",
          url: "http://127.0.0.1:8000/home/date/",
        }).then(function(resp) {
          console.log(resp.data);
          setMonths(resp.data.map(date=><option id={nanoid()} value={date.month}>{date.month}</option>));
          setYears(resp.data.map(date=><option id={nanoid()} value={date.year}>{date.year}</option>));
          
        });
      },[]);

    function getMembers(){
        axios({
            method: "get",
            url: "http://127.0.0.1:8000/home/member/"+selectedyear+"/"+selectedmonth,
          }).then(function(resp) {
            console.log(resp.data);
            setRows(resp.data.map(member=>
                <tr>
                    <td>{member.maintask}</td>
                    <td>{member.subtask}</td>
                    <td>{member.email}</td>
                    <td>{member.actualcount}</td>
                    <td>{member.actualeffort}</td>
                </tr>
            ));
          });
    }


    return (
        <div>
            <h1>Date</h1>
            <select onChange={(e)=>{setSelectedMonth(e.target.value);}}>
                <option>---</option>
                {months}
            </select>
            <select onChange={(e)=>{setSelectedYear(e.target.value);}}>
                <option>---</option>
                {years}
            </select>
            <button onClick={getMembers}>Go</button>

            <Table striped bordered hover>
                <thead>
                    <th>Maintask</th>
                    <th>Subtask</th>
                    <th>Email</th>
                    <th>Actual Count</th>
                    <th>Actual Efforts</th>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </Table>
        </div>

    );
}
